import axios  from "axios";

export const getArticles =async (req) => {
    const constructHeaders = () => ({
      "Content-Type": "application/json",
    });
    const response=await axios.get(`https://dev.bestrealestateblog.com/wp-json/wp/v2/posts?per_page=${req.perPage}&page=${req.pageNum}&_embed`,{
      headers: constructHeaders(),
    });
    return response
  };

  export const getSingleArticle =async (slug) => {
    const constructHeaders = () => ({
      "Content-Type": "application/json",
    });
    const response=await axios.get(`https://dev.bestrealestateblog.com/wp-json/wp/v2/posts/?slug=${slug}`,{
      headers: constructHeaders(),
    });
    return response
  };