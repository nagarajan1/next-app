import Link from "next/link";
import { useRouter } from "next/router";
import Head from "next/head";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
const article = ({ article }) => {
//   const router = useRouter();
//   const { id } = router.query;
//   console.log("id", id, router,article);
  return (
    <>
      <Head>
        <title>{article[0].title.rendered}</title>
        {ReactHtmlParser(article[0].yoast_head)}
      </Head>
      <h1>{article[0].title.rendered}</h1>
      <div>
      {ReactHtmlParser(article[0].content.rendered)}
      </div>
      {/* <p>{article.body}</p> */}
      <br />
      <Link href="/">Go Back</Link>
    </>
  );
};

export const getStaticProps = async (context) => {
  const res = await fetch(
    `https://dev.bestrealestateblog.com/wp-json/wp/v2/posts/?slug=${context.params.id}`
  );
  const article = await res.json();
  console.log('articles',article)
  return {
    props: {
      article,
    },
  };
};

export const getStaticPaths = async () => {
  const res = await fetch(
    `https://dev.bestrealestateblog.com/wp-json/wp/v2/posts`
  );

  const articles = await res.json();

  const ids = articles.map((article) => article.slug);
  const paths = ids.map((id) => ({ params: { id: id.toString() } }));

  return {
    paths,
    fallback: false,
  };
};

// export const getStaticProps = async (context) => {
//   const res = await fetch(
//     `https://jsonplaceholder.typicode.com/posts/${context.params.id}`
//   )

//   const article = await res.json()

//   return {
//     props: {
//       article,
//     },
//   }
// }

// export const getStaticPaths = async () => {
//   const res = await fetch(`https://jsonplaceholder.typicode.com/posts`)

//   const articles = await res.json()

//   const ids = articles.map((article) => article.id)
//   const paths = ids.map((id) => ({ params: { id: id.toString() } }))

//   return {
//     paths,
//     fallback: false,
//   }
// }

export default article;
