import React, { Component } from "react";
import styled, { ThemeProvider } from "styled-components";
import Head from "next/head";
import Link from "next/link";

import { getArticles } from "../Utils/apiCalls";

const H2 = styled.h2`
  color: red;
  font-size: 40px;
`;

export default class demo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isScrollDown: false,
      articlesReq: {
        perPage: 9,
        pageNum: 1,
      },
      articleData: [],
      totalDatas: 0,
      numberOfPages: 0,
      apiLoading: false,
    };
  }

  componentDidMount() {
    this.getPageData();
  }

  getPageData = async () => {
    const { articlesReq } = this.state;
    const { data } = await getArticles(articlesReq);
    //  set ARTCLE DATAS
    this.setState({
      articleData: data,
    });
  };

  render() {
    const { articleData } = this.state;
    return (
      <div>
        <Head>
          <title>Demo pages</title>
          <meta name="description" content="Find out now." />
          <meta property="og:locale" content="en_US" />
          <meta property="og:type" content="article" />
          <meta
            property="og:title"
            content="What&#039;s Your Home Worth In Today&#039;s Market?"
          />
          <meta property="og:description" content="Find out now." />
          <meta
            property="og:url"
            content="https://brebuat.bestrealestateblog.com/home/"
          />
          <meta property="og:site_name" content="The Best Real Estate Blog" />
          <meta
            property="article:publisher"
            content="https://www.facebook.com/lightersideofrealestate/"
          />
          <meta
            property="article:modified_time"
            content="2017-05-16T02:53:43+00:00"
          />
          <meta
            property="og:image"
            content="https://brebuat.bestrealestateblog.com/wp-content/uploads/2016/10/home-worth.jpg"
          />
          <meta property="og:image:width" content="700" />
          <meta property="og:image:height" content="366" />
          <meta name="twitter:card" content="summary_large_image" />
          <meta
            name="twitter:title"
            content="What&#039;s Your Home Worth In Today&#039;s Market?"
          />
          <meta name="twitter:description" content="Find out now." />
          <meta
            name="twitter:image"
            content="https://brebuat.bestrealestateblog.com/wp-content/uploads/2016/10/home-worth.jpg"
          />
          <meta name="twitter:site" content="@lightersideofre" />
        </Head>
        <main>
          <h1>Demo page</h1>
          {/* <H2>Sub hewader</H2> */}
        </main>
        {articleData && articleData.length > 0 ? (
          articleData.map((x, i) => {
            return (
              <Link key={i} href={`/article/${x.slug}`}>
                <H2 >{x.title.rendered}</H2>
              </Link>
            );
          })
        ) : (
          <H2>No data</H2>
        )}
      </div>
    );
  }
}
